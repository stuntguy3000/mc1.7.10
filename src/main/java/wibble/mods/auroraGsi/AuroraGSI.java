package wibble.mods.auroraGsi;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Logger;

@Mod(modid = AuroraGSI.MODID, useMetadata = true)
public class AuroraGSI {

    public static final String MODID = "auroragsi";

    public static Configuration config;
    public static int AuroraPort = 9088;
    public static int UpdateRate = 100;

    private static Logger logger;

    /**
     * Grab a reference to the logger, and show a warning on servers that the mod isn't needed.
     */
    @EventHandler
    public void preInit(FMLPreInitializationEvent evt) {
        logger = evt.getModLog();
        MinecraftForge.EVENT_BUS.register(this); // Required to listen for config event

        config = new Configuration(evt.getSuggestedConfigurationFile());
        syncConfig();

        if (!FMLCommonHandler.instance().getSide().isClient())
            logger.warn("There is no need for Aurora GSI to be installed on the server.");
    }

    /**
     * Start a timer that will send a request to the Aurora HTTP listening server containing the game data.
     */
    @EventHandler
    @SideOnly(Side.CLIENT)
    public void init(FMLInitializationEvent evt) {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new SendGameState(), 0, UpdateRate, TimeUnit.MILLISECONDS);
    }

    /**
     * Load the configuration file.
     */
    public static void syncConfig() {
        try {
            // Load config
            config.load();

            // Read props from config
            AuroraPort = config.get(Configuration.CATEGORY_GENERAL, "AuroraPort", "9088", "This should probably not need to be changed! The target port that the GSI updates should be POSTed to. Should match the port that Aurora is listening on.").getInt(9088);
            UpdateRate = config.get(Configuration.CATEGORY_GENERAL, "UpdateRate", "100", "The rate at which the Aurora GSI mod sends data to Aurora application. The number of updates per second can be calculated by taking 1000 and dividing it by the number here. E.G. update rate of 100 means 10 updates per second.").getInt(100);
        } finally {
            // Save props to config IF config changed
            if (config.hasChanged())
                config.save();
        }
    }
}
